# Python behavior for an Alluvion Box.
# This is just a basic box to test with

from pycel import *
from AlluvionItem import *

class AlluvionTestBox (AlluvionItem):
	
	def __init__ (self, celEntity):
		print "Intializing TestBox...", celEntity.Name
		self.real_init (celEntity)

	def pcmeshsel_down (self, celEntity, args):
		self.pickup (celEntity, args)
