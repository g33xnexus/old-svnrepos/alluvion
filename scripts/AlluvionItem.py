# Python behavior for an Alluvion Item.
#	Ultimately every item in the game should be drived from this class.

from pycel import *

class AlluvionItem:
	
	def __init__ (self, celEntity):
		print "Intializing item...", celEntity.Name
		self.mesh = None

	def real_init (self, celEntity):
		# This handles the real initalization process.
		# Should be called by every derivative of this class, since this base class has no mesh

		#Getting the mesh
		self.mesh = celGetMesh(celEntity)
		if self.mesh:
			self.meshobj = self.mesh.Mesh

		self.pccamera = celGetDefaultCamera (Entities['camera'])
		self.pcmeshsel = celAddMeshSelect (celEntity)
		self.pcmeshsel.SetCamera  (self.pccamera)
		
	def activate (self):
		pass

	def pcmeshsel_up (self, celEntity, args):
		pass

	def pcmeshsel_down (self, celEntity, args):
		pass

	def pickup (self, celEntity, item, args):
		#Note, if we don't have a mesh we shouldn't be doing anything, right?
		if self.mesh:
			pcinv = celGetInventory (Entities['camera'])
			if pcinv:
				pcinv.AddEntity (celEntity)
				item.mesh.Hide()

