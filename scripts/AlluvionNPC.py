# a "behaviour" for an npc with different flavours, controlled by the "chosen"
# pcproperty, with the following values:
# - 0: will walk and stop randomly. (default)
# - 1: will stay completely still.
# . 2: will walk reandomly (doesnt stop).
# also can control simple actions on an spr3d.
# uses: linmove, timer, mesh, properties
from pycel import *

import math

##################
class AlluvionNPC:

	def __init__ (self, celEntity):
		print "Initializing npc...", celEntity.Name

		self.ent = celEntity
		self.goals = list ()
		self.actions = list ()
		self.playerEntity = Entities["camera"]

		# get or create linmove
		self.linmove = celLinearMovement (celEntity)

		# set timer if there is not one
		self.timer = celGetTimer (celEntity)
		if not self.timer:
			self.timer = celAddTimer (celEntity)
			self.timer.WakeUp (500, True)

		# check for mesh
		self.mesh = celGetMesh (celEntity)
		if self.mesh:
			self.meshobj = self.mesh.Mesh

		self.spr3dstate = SCF_QUERY_INTERFACE (self.meshobj.GetMeshObject (), iSprite3DState)
		if self.spr3dstate:
			self.spr3dstate.SetAction ("stand")
		else:
			self.spr3dstate = None

		# Add an initial goal
		self.addGoal (self.DestinationGoal (self, 1, self.playerEntity))
		
		# Mesh select stuff in progress

		self.pccamera = celGetDefaultCamera(self.playerEntity)
		self.pcmeshsel = celAddMeshSelect (celEntity)
		self.pcmeshsel.SetCamera (self.pccamera)

	def real_init (self, celEntity, room):
		pass

	def addGoal (self, goal):
		self.goals.append (goal)

	def pcmeshsel_up (self, celEntity, args):
		print "In pcmeshsel_up"
		pl = physicallayer_ptr
		actor = Entities["camera"]
		toolt = celGetToolTip (actor)
		if toolt:
			toolt.Text = celEntity.Name

	def pcmeshsel_down (self, celEntity, args):
		print "In pcmeshsel_down"
	
	def pcmeshsel_move (self, celEntity, args):
		print "In pcmeshsel_move"

	def pctimer_wakeup (self, celEntity, args):
		# Activate actions for highest-priority goals
		print "In pctimer_wakeup!"
		while len (self.actions) > 0:
			action = self.actions.pop ()
			action.reset ()

		self.goals.sort (cmp=lambda x,y: cmp(x.priority, y.priority))
		for goal in self.goals:
			requirementsMet = True
			(requiredActions, desiredActions) = goal.getActionClasses ()
			for action in requiredActions:
				if (action.locked == True):
					requirementsMet = False
					break
			if requirementsMet:
				(requiredActions, desiredActions) = goal.getActions ()
				for action in requiredActions:
					action.activate ()
					self.actions.append (action)
				for action in desiredActions:
					if (action.locked != True):
						action.activate ()
						self.actions.append (action)


	######################
	class DestinationGoal:
		def __init__ (self, npc, priority, target):
			self.npc = npc
			self.priority = priority
			self.target = target

		def getActionClasses (self):
			# (required, desired)
			return ((AlluvionNPC.MoveAction,), (AlluvionNPC.RotateAction,))

		def getActions (self):
			# (required, desired)
			return ((AlluvionNPC.MoveAction (self.npc, self.target),), (AlluvionNPC.RotateAction (self.npc, self.target),))


	#################
	class MoveAction:
		locked = False

		def __init__ (self, npc, destination):
			self.npc = npc

			if ('GetMovable' in dir(destination)):
				self.destination = destination.GetMovable ()
			elif ('GetMesh' in dir(destination)):
				self.destination = destination.GetMesh ().GetMovable ()
			elif ('GetPropertyClassList' in dir(destination)):
				mesh = celGetMesh (destination)
				self.destination = mesh.GetMesh ().GetMovable ()
			else:
				self.destination = destination

			self.meshobj = npc.meshobj
			self.linmove = npc.linmove
			self.linmove.SetVelocity (csVector3 (0, 0, 0))

		def calculateVelocity (self):
			pos = csVector3 ()
			pos = self.meshobj.GetMovable ().GetFullPosition ()
			trans = self.meshobj.GetMovable ().GetFullTransform ()

			if ('GetPosition' in dir(self.destination)):
				target = self.destination.GetPosition ()
			else:
				target = self.destination

			direction = target - pos

			# Get the direction in local coordinates instead of world.
			direction = trans.Other2This (direction)

			# This class only handles X and Z (horizontal) movement, not jumping.
			direction.y = 0

			distance = direction.Norm ()
			direction.Normalize ()
			if distance > 10:
				return 0.125 * direction
			else:
				return 0.125 * direction * (distance / 10)

		def reset (self):
			AlluvionNPC.MoveAction.locked = False

			self.linmove.SetVelocity (csVector3 (0, 0, 0))

			if self.npc.spr3dstate:
				self.npc.spr3dstate.SetAction ("stand")

		def activate (self):
			if AlluvionNPC.MoveAction.locked == True:
				return False
			AlluvionNPC.MoveAction.locked = True

			self.linmove.SetVelocity (self.calculateVelocity ())

			if self.npc.spr3dstate:
				self.npc.spr3dstate.SetAction ("walk")


	###################
	class RotateAction:
		locked = False

		def __init__ (self, npc, lookAt):
			self.npc = npc

			if ('GetMovable' in dir(lookAt)):
				self.lookAt = lookAt.GetMovable ()
			elif ('GetMesh' in dir(lookAt)):
				self.lookAt = lookAt.GetMesh ().GetMovable ()
			elif ('GetPropertyClassList' in dir(lookAt)):
				mesh = celGetMesh (lookAt)
				self.lookAt = mesh.GetMesh ().GetMovable ()
			else:
				self.lookAt = lookAt

			self.meshobj = npc.meshobj
			self.linmove = npc.linmove
			vel = csVector3 ()
			self.linmove.GetAngularVelocity (vel)
			vel.y = 0
			self.linmove.SetAngularVelocity (vel)

		def calculateAngleVelocity (self):
			pos = csVector3 ()
			pos = self.meshobj.GetMovable ().GetFullPosition ()
			front = self.meshobj.GetMovable ().GetFullTransform ().GetFront ()
			yrot = math.atan (front.z / front.x)

			if ('GetPosition' in dir(self.lookAt)):
				target = self.lookAt.GetPosition ()
			else:
				target = self.lookAt

			direction = target - pos
			destAngle = math.atan (direction.z / direction.x)
			return (destAngle, 0.125 * ((destAngle - yrot) / (abs (destAngle - yrot))))

		def reset (self):
			AlluvionNPC.RotateAction.locked = False

			vel = csVector3 ()
			self.linmove.GetAngularVelocity (vel)
			vel.y = 0
			self.linmove.SetAngularVelocity (vel)

		def activate (self):
			if AlluvionNPC.RotateAction.locked == True:
				return False
			AlluvionNPC.RotateAction.locked = True

			vel = csVector3 ()
			dest = csVector3 ()
			self.linmove.GetAngularVelocity (vel)
			(dest.y, vel.y) = self.calculateAngleVelocity ()
			self.linmove.SetAngularVelocity (vel, dest)
			pass


	################
#	class AimAction:
#		locked = False
#
#		def __init__ (self, npc, lookAt):
#			self.npc = npc
#
#			self.lookAt = lookAt
#
#			self.linmove = npc.linmove
#			self.linmove.SetAngularVelocity ((0, 0, 0))
#
#		def reset (self):
#			AlluvionNPC.AimAction.locked = False
#
#			self.linmove.SetAngularVelocity ((0, 0, 0))
#
#		def activate (self):
#			if AlluvionNPC.AimAction.locked == True:
#				return False
#			AlluvionNPC.AimAction.locked = True
#
#			self.linmove.SetAngularVelocity (vel)
#			pass

